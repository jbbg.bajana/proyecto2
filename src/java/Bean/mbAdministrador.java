/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author DELL
 */
@Named(value = "mbAdministrador")
@RequestScoped
public class mbAdministrador {

    /**
     * Creates a new instance of mbAdministrador
     */
    private String nombre;
    private String password;
    
    public mbAdministrador() {
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String acceder(){
    if (this.nombre.equals("Admin")){
         return "Ingreso admitido";}
    else{
         return "Ingreso denegado";}
    } 
    
}
