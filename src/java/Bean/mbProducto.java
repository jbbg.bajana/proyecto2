/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.ProductoDAO;
import Dao.ProductoDAOImpl;
import Modelo.CategoriaProducto;
import Modelo.Producto;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author keysw
 */
@Named(value = "mbProducto")
@ViewScoped
public class mbProducto implements java.io.Serializable{

    private Producto obj_Producto;
    private List<Producto> lstProductos;
    
    public mbProducto() {
    }

    public Producto getObj_Producto() {
        return obj_Producto;
    }

    public void setObj_Producto(Producto obj_Producto) {
        this.obj_Producto = obj_Producto;
    }
    
    public List<Producto> getLstProducto(){
        ProductoDAO obj_ProductoDao = new ProductoDAOImpl();
        lstProductos = obj_ProductoDao.consultarProducto();
        return lstProductos;
        
    }
    
    public List<Producto> getLstProductoCategoria(int id_cat){
        ProductoDAO obj_ProductoDao = new ProductoDAOImpl();
        return obj_ProductoDao.consultarProductoCategoria(id_cat);
    }
    
    public List<Producto> getLstVideojuegos(){
        ProductoDAO obj_ProductoDao = new ProductoDAOImpl();
        return obj_ProductoDao.consultarVideojuegos();
    }
    
    public List<Producto> getLstAccesorios(){
        ProductoDAO obj_ProductoDao = new ProductoDAOImpl();
        return obj_ProductoDao.consultarAccesorios();
    }
    
    public List<Producto> getLstConsolas(){
        ProductoDAO obj_ProductoDao = new ProductoDAOImpl();
        return obj_ProductoDao.consultarConsolas();
    }
    
    public void agregarProducto(){        
        ProductoDAO productoDAO = new ProductoDAOImpl();
        productoDAO.agregarProducto(obj_Producto);
        obj_Producto = new Producto();
        
     }

    public void modificarProducto(){
        ProductoDAO productoDAO = new ProductoDAOImpl();
        productoDAO.modificarProducto(obj_Producto);
        obj_Producto = new Producto();
     }

      public void eliminarProducto(){
          ProductoDAO productoDAO = new ProductoDAOImpl();
        productoDAO.eliminarProducto(obj_Producto);
        obj_Producto = new Producto();
      }
        
}
